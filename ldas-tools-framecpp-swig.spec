# -*- mode: rpm-spec; indent-tabs-mode: nil -*-
#========================================================================
%global debug_package %{nil}

%define    ldastoolsal_swig_ver      2.6.10
%define    basename_ver         2.8.2
%define    basename_   ldas-tools-framecpp
%define    name        %{basename_}-swig
%define    name_desc   frameCPP
%define    hdr_dir     framecpp
%define    tarbasename ldas-tools-framecpp-swig
%define    version     2.6.13
%define    release     1.1
%define    _docdir     %{_datadir}/doc/%{name}-%{version}

%define check_cmake3  ( 0%{?rhel} && 0%{?rhel} <= 7 )
%define check_python3 ( 0%{?__python3:1} || ( 0%{?rhel} >= 7 ) )

#------------------------------------------------------------------------
Name:      %{name}
Summary:   SWIG bindings for LDAS Tools frameCPP library
Version:   %{version}
Release:   %{release}%{?dist}
License:   GPLv2+
URL:       "https://wiki.ligo.org/Computing/LDASTools"
Group:     Application/Scientific
Conflicts: %{basename_}-devel < 2.6.0
Prefix:    %_prefix
BuildRoot: %{buildroot}
Source0:   https://software.igwn.org/lscsoft/source/%{tarbasename}-%{version}.tar.gz

#------------------------------------------------------------------------
%if %{check_cmake3}
BuildRequires: cmake3 >= 3.6
BuildRequires: cmake
%else
BuildRequires: cmake >= 3.6
%endif
BuildRequires: python3-rpm-macros
BuildRequires: gawk
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: make
Buildrequires: ldas-tools-cmake >= 1.2.3
Buildrequires: %{basename_}-devel >= 2.8.2
Buildrequires: pkgconfig
BuildRequires: rpm-build
Buildrequires: swig
#........................................................................
# Python 3 dependencies
#........................................................................
%if %check_python3
BuildRequires: python3
BuildRequires: python3-libs
BuildRequires: python3-devel
BuildRequires: python%{python3_pkgversion}-numpy
BuildRequires: python%{python3_pkgversion}-ldas-tools-al >= 2.6.10
%endif
#........................................................................
# Runtime dependencies
#........................................................................
Requires: swig
Requires: %{basename_}-devel >= 2.8.2

%description


#========================================================================
%if %check_python3
%package -n python%{python3_pkgversion}-%{basename_}
Summary: LDAS Tools %{name_desc} toolkit python %{python3_version} bindings
#........................................................................
# Runtime dependencies
#........................................................................
Requires: python%{python3_pkgversion}-ldas-tools-al >= %{ldastoolsal_swig_ver}
Requires: %{basename_} >= %{basename_ver}
Requires: python%{python3_pkgversion}-numpy
%{?python_provide:%python_provide python%{python3_pkgversion}-%{basename_}}

%description -n  python%{python3_pkgversion}-%{basename_}
This provides the python%{python3_version} bindings for the %{name_desc} library
%endif

%prep
%setup -c -T -D -a 0 -n %{name}-%{version}

%build
%if %check_python3
export PYTHON3_BUILD_OPTS="-DENABLE_SWIG_PYTHON3=YES -DPYTHON3_EXECUTABLE=%{__python3}"
%else
export PYTHON3_BUILD_OPTS="-DENABLE_SWIG_PYTHON3=NO"
%endif

%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    ${PYTHON3_BUILD_OPTS} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    %{tarbasename}-%{version}

%install
make install DESTDIR=%{buildroot}

%check
%if %{check_cmake3}
export CTEST_PROGRAM=ctest3
%else
export CTEST_PROGRAM=ctest
%endif
${CTEST_PROGRAM} -V %{?_smp_mflags}

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_includedir}/%{hdr_dir}

%if %check_python3
%files -n python%{python3_pkgversion}-%{basename_}
%defattr(-,root,root,-)
%{_libdir}/python3*
%{_bindir}/py3*
%endif

%changelog
* Fri Jun 17 2022 Edward Maros <ed.maros@ligo.org> - 2.6.13-1
- Built for new release

* Wed Sep 29 2021 Edward Maros <ed.maros@ligo.org> - 2.6.12-1
- Built for new release

* Fri Aug 13 2021 Edward Maros <ed.maros@ligo.org> - 2.6.11-1
- Built for new release

* Tue Mar 9 2021 Edward Maros <ed.maros@ligo.org> - 2.6.10-1
- Built for new release

* Wed Aug 14 2019 Edward Maros <ed.maros@ligo.org> - 2.6.9-1
- Built for new release

* Sat Feb 02 2019 Edward Maros <ed.maros@ligo.org> - 2.6.8-1
- Built for new release

* Tue Jan 08 2019 Edward Maros <ed.maros@ligo.org> - 2.6.7-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
